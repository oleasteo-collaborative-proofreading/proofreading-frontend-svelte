import { SvelteComponentDev } from "svelte/internal";

export type CheckActive = (list: (string | SvelteComponentDev)[]) => boolean;

export type Page = (params?: any) => {
  component: new (options: any) => SvelteComponentDev,
  props?: any,
};

export interface Pages {
  [key: string]: Page
}
