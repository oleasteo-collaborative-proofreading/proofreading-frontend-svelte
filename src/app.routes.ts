import { Error404 } from "modules/error";
import { Landing }  from "modules/landing";

import { Page, Pages } from "modules/routing/types";

export const PAGES: Pages = {
  "": () => ({
    component: Landing,
  }),
  "documents": () => ({
    component: Landing,
  }),
  "document": () => ({
    component: Landing,
  }),
  "document/:id": ({ id }) => ({
    component: Landing,
    props: { id },
  }),
};

export const PAGE_404: Page = () => ({
  component: Error404,
});
